#!/usr/bin/env bash

myname="$(basename "$0")"

shutdown=yes
if [ "$1" == '--no-shutdown' ]
then
    shutdown=no
    shift
fi

if [ $# -ne 3 ]
then
    echo "Usage: $myname [--no-shutdown] <image_path> <quality> <output_path>"
    exit 1
fi

die()
{
    echo 1>&2 "ERROR: $*"
    exit 1
}

image_path="$1"
quality="$2"
output_path="$3"

if [ ! -d "$image_path" ]
then
    die "Directory '$image_path' does not exist."
fi

case "$quality" in
    high|medium|low) ;;
    *) die "<quality> argument must be one of high, medium or low." ;;
esac

if [[ "$output_path" != gs://* ]]
then
    die "<output_path> argument must point to a GCP (gs://) bucket or its
subdirectory, where you have write access enabled for your
configuration of gsutil"
fi

echo "$myname $*" > flow_progress
if ! gsutil -q cp flow_progress "$output_path"/flow_progress
then
    die "Cannot write files into $output_path/"
fi

progress_file="$(pwd)"/flow_progress
update_progress()
{
    echo "$@" | tee -a "$progress_file"
    gsutil -q cp flow_progress "$output_path"/flow_progress
}

set -x
update_progress "3D reconstruction start time: $(date)"

run_colmap()
{
    set -o pipefail
    colmap automatic_reconstructor          \
        --image_path "$image_path"          \
        --workspace_path .                  \
        --vocab_tree_path /usr/share/colmap/vocab_tree-65536.bin    \
        --single_camera 1                   \
        --quality "$quality"                \
        --sparse 1                          \
        --dense 1                           \
        --use_gpu 1                         \
    |& tee reconstruction.log
}

if run_colmap
then
    update_progress "3D reconstruction completed successfully"
    reconstruction_failed=0
else
    update_progress "3D reconstruction failed"
    reconstruction_failed=1
fi

update_progress "3D reconstruction end time: $(date)"

gzip -c --best reconstruction.log > reconstruction.log.gz
gsutil -q cp reconstruction.log.gz "$output_path"/

update_progress "Uploaded reconstruction.log.gz."

if [ "$reconstruction_failed" -eq 0 ]
then
    # Change to the last dense workspace directory
    cd dense
    reconstruction_id="$(ls|sort -n|tail -1)"
    cd "$reconstruction_id"

    gzip -c --best meshed.ply > mesh.ply.gz
    gsutil -q cp mesh.ply.gz "$output_path"/


    cd ../..
    colmap model_converter                              \
            --input_path sparse/"$reconstruction_id"    \
            --output_path bundler                       \
            --output_type Bundler                       \

    n="$(wc -l < bundler.list.txt)"
    head -n $((n*5+2)) bundler.bundle.out | sed -e '2 s/[0-9]*$/0/' > bundler.cameras.out

    gsutil -q cp bundler.list.txt bundler.cameras.out "$output_path"/

    update_progress "Uploaded Bundler project. Shutting down."
fi

if [ "$shutdown" == no ]
then
    exit
fi

sudo shutdown now
