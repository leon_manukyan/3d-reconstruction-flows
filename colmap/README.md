# Introduction

This project provides several scripts for facilitating the setup and running
of [COLMAP](https://colmap.github.io) in the Google Cloud, taking advantage of
GPU instances.

Currently the scripts are hardcoded for Nvidia Tesla K80 GPUs, but can be
extended so that the type of the GPU is controlled from the command line.

Besides, the 3D reconstruction options used in the `run_3d_reconstruction`
script require that all images are taken with the same camera/lens combination,
at the same resolution/size settings, and using the same focal length (i.e.
without zooming in/out).

# Sample flow of running 3D reconstruction via COLMAP in the Google cloud

## Before you start

1. You must have a paid Google Cloud Platform account (free accounts are not eligible for GPUs).
2. You must have an active project.
3. You must create at https://console.cloud.google.com/iam-admin/serviceaccounts a service
   account for your project (or you may use the Compute Engine default service account) and
   you must create a key for that service account and store it securely.
4. You must have the `gcloud` suite installed. The easiest way is to use the
   [Google Cloud Shell](https://cloud.google.com/shell/) or launch an f1-micro instance, both
   of which come with `gcloud` suite preinstalled. To configure it for your project, run:

        gcloud auth activate-service-account --key-file PATH_TO_YOUR_KEY_FILE

    where `PATH_TO_YOUR_KEY_FILE` must be replaced with the path to the key file created on the
    previous step (if you decided to run `gcloud` in the Google Cloud, then you must first upload
    there the service account key file).

5. You must set the `gcloud compute` zone to one containing Tesla K80 GPUs. One such zone is **us-east1-c**:

        gcloud config set compute/zone us-east1-c

    The availability of GPUs by zones can be checked as follows:

        gcloud compute accelerator-types list

6. You must have a positive Tesla K80 GPU quota for your project in the selected zone (you may need
   to file a quota increase request).

7. You must create a [Google Cloud Storage](https://cloud.google.com/storage/docs/) bucket where your
   3D reconstruction results will be saved.

8. Assuming that this repository was cloned in the current directory:

        git clone --depth 1 https://bitbucket.org/leon_manukyan/3d-reconstruction-flows

## Create a COLMAP VM image/snapshot

First of all you must create an image (or, more accurately, a snapshot) of a VM with
COLMAP installed on it. This must be performed once. Then you can launch new VMs
from that image/snapshot which is faster and, therefore, cheaper.

Now it's done with a single command:

    3d-reconstruction-flows/colmap/create_gcloud_vm_image prebuilt-colmap

## Run 3D reconstruction using a prebuilt COLMAP VM

Part of the actions below are for adding and formatting a disk that
will hold the data of the (possibly multiple) reconstruction runs. Those actions
should be performed only once per disk. For more details see
https://cloud.google.com/compute/docs/disks/add-persistent-disk.

    # Create a disk for the reconstruction data.
    # This must be performed once. Then you can reuse this disk
    # as much as you want with different VMs.
    gcloud compute disks create data --size 200GB --type pd-standard

    # Launch a virtual machine in the Google cloud from an earlier snapshot.
    # The virtual machine name is colmap-vm, it has 8 CPUs, 52GB of RAM,
    # and 4 Nvidia Tesla K80 GPUs
    3d-reconstruction-flows/gcloud_utils/create_vm_from_snapshot colmap-vm prebuilt-colmap 8 52GB 4

    # Attach the disk to the VM
    gcloud compute instances attach-disk colmap-vm --disk=data --device-name=sdb

    # Connect to the VM
    gcloud compute ssh colmap-vm

    #####################################################################################
    # Subsequent commands are executed on the VM
    #####################################################################################

    # Format the disk. Again, this must be performed only once (after you've attached
    # the disk for the first time)
    sudo mkfs.ext4 -m 0 -F -E lazy_itable_init=0,lazy_journal_init=0,discard /dev/sdb

    # Mount the disk to the VM
    sudo mount -o discard,defaults /dev/sdb /data

    # Run this once (only after you just formatted your disk)
    sudo chmod a+w /data

    cd /data
    mkdir dataset_name
    cd dataset_name
    mkdir images

    # Download the photos into the images directory using gsutil, wget, scp or any other means
    # of delivering data to the VM

    mkdir run1.medium_quality
    cd run1.medium_quality

    # If you connect to VM with a different username than was used during the COLMAP
    # installation stage, then you can look up for the 3d-reconstruction-flows directory
    # under other users' home directories. Alternatively, you can again clone the
    # 3d-reconstruction-flows repository.
    ~/3d-reconstruction-flows/colmap/run_3d_reconstruction ../images medium gs://mybucket/dataset.medium_quality_results

    # After the run finishes all output will be put into the specified bucket and the VM
    # will automatically shut down. If you don't want the VM to shut down, add the
    # --no-shutdown option to the above command. Note however that GPU-equipped instances
    # are quite costly, so having them shut down automatically as soon as possible may
    # save you a lot of money (especially if you are not going to wait for a long running 3D
    # reconstruction flow to complete and may leave home or go to bed).

    # Remove the virtual machine
    gcloud compute instances delete colmap-vm


# Scripts

## colmap/create_gcloud_vm_image

The `create_gcloud_vm_image` script creates in the Google Cloud an image of an Ubuntu 16.04 VM
with COLMAP installed on it.

Usage: **`colmap/create_gcloud_vm_image`** *`image_name`*

where

- *`image_name`* may consist only of lowercase letters, digits and hyphens;
    it must start with a letter, and may not end with a hyphen

Examples:

       colmap/create_gcloud_vm_image colmap-20180123

## colmap/setup

Sets up (builds and installs) COLMAP on a fresh Ubuntu 16.04 machine. You shouldn't use
this script directly - it is called by the `colmap/create_gcloud_vm_image` script.

This script was tested on a maching running Ubuntu 16.04 with a Nvidia Tesla
K80 GPU.  With minor modificationsm it should also work for newer versions of
Ubuntu (or other similar systems). Most probably it will work for other
Nvidia GPUs without any modifications (that part should be taken care of
by the Nvidia CUDA toolkit).

Usage: **`colmap/setup`**


## colmap/run_3d_reconstruction

Runs fully automatic 3D reconstruction and uploads the results to the specified Google
Cloud Storage bucket. Must be executed on a VM. Upon completion shuts down
the VM unless the **`--no-shutdown`** option was provided.

Usage: **`colmap/run_3d_reconstruction`** `[`**`--no-shutdown`**`]` *`image_path quality output_path`*

where

- *`image_path`* is the path of the directory containing the input images
- *`quality`* is one of **high**, **medium**, or **low**
- *`output_path`* is the **`gs://`** URL of a Google Cloud Storage bucket
    or its subdirectory. On the VM `gsutil` must be preconfigured so that it has
    write access to the specified bucket. One way of doing this is to run (on the VM)
    `gsutil config -e` and supply to it the private key of the Compute Engine default
    service account that you can create and download from https://console.cloud.google.com/iam-admin/serviceaccounts

Restrictions:

- All images must be taken with the same camera/lens combination, at the same
    resolution/size settings, using the same focal length (i.e. without zooming in/out).
