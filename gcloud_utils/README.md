## gcloud_utils/create_vm

The `gcloud_utils/create_vm` script starts a new Ubuntu 16.04 VM with the specified number of CPUs/GPUs and RAM
and executes the specified setup commands on it.

Usage: **`gcloud_utils/create_vm`** *`instance_name cpu_count memory gpu_count`* `[`*`setup_cmds ...`*`]`

where

- *`intance_name`* may consist only of lowercase letters, digits and hyphens;
      it must start with a letter, and may not end with a hyphen

- *`memory`* must be an integer followed by an optional GB or MB suffix

- *`setup_cmds`* is an optional list of commands to be executed on the
      created VM. Setup scripts from this repository can be referred by
      their paths relative to the repository root.

Examples:

    gcloud_utils/create_vm colmap-vm 4 16GB 4 colmap/setup

    gcloud_utils/create_vm dense-reconstruction 2 8000MB 0 openmvs/setup

## gcloud_utils/create_vm_from_snapshot

The `gcloud_utils/create_vm_from_snapshot` script creates a boot disk from the named snapshot
and boots a new VM instance from it.

Usage: **`create_vm_from_snapshot`** *`instance_name snapshot_name cpu_count memory gpu_count`*

where

- *`intance_name`* may consist only of lowercase letters, digits and hyphens;
    it must start with a letter, and may not end with a hyphen

- the snapshot *`snapshot_name`* must be taken from a bootable disk. It doesn't
    need to be a snapshot of the VM created by the `gcloud_utils/create_vm` script.

- *`memory`* must be an integer followed by an optional GB or MB suffix


Examples:

       gcloud_utils/create_vm_from_snapshot colmap-vm prebuilt-colmap-20180521-2230 4 16GB 4

       gcloud_utils/create_vm_from_snapshot dense-reconstruction colmap-v3-4 2 8000MB 1



