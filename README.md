# What is this project about

3D reconstruction from a set of high-resolution images is a compute
intensive operation that requires powerful hardware. With the advent of
cloud computing and in the era of open source software now anyone
can perform 3D reconstruction without upfront major investment into
a high-end workstation/server and expensive commercial software.

This project's goal is to lower the effort, time and, hence, cost required
to get started with 3D reconstruction in the cloud.

# What flows are currently supported

- Pure [COLMAP](https://colmap.github.io) in the [Google Cloud](https://cloud.google.com/). See [colmap/README](colmap/README.md).